#+TITLE: My nextcloud configuration
#+AUTHOR: Daniel Laguna Soto
#+EMAIL: mail@dani.codes

#+Options: toc:nil date:nil email:t h:3

This repository contains the configuration I use to run my personal Nextcloud
instance. At the time I am writing this lines everything runs in a Raspberry Pi
4.

There are two main folders in this repository:

1. [[file:nextcloud/]] contains the containerized Nextcloud configuration
2. [[file:scripts/]] contains useful scripts for the host
