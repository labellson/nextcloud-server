#!/usr/bin/env bash
set -euo pipefail

# backup variables
BACKUP_SRC="/mnt/hdd0/nextcloud-bck/"

# retain only the last 30 backups and remove "latest" from the list as it is a
# symbolic link
OLD_BACKUPS=$(find ${BACKUP_SRC} -mindepth 1 -maxdepth 1 -type d | sort --reverse | tail -n +31)

# if the list is not empty delete the old files
[ -n "${OLD_BACKUPS}" ] && rm -rf ${OLD_BACKUPS}
