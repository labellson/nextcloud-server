#!/usr/bin/env bash
set -euo pipefail

HOST="labellson@192.168.178.50"
BACKUP_SRC="/mnt/hdd0/"
BACKUP_DST="/mnt/hdd0/"
PARTIAL_DIR="/mnt/hdd0/.rsync-partial"
EXCLUDE_DIR="/mnt/hdd0/lost+found"

# backup replica of SRC to DST
rsync -Aavxh \
    --delete-after \
    --partial-dir ${PARTIAL_DIR} \
    --exclude ${EXCLUDE_DIR} \
    ${HOST}:${BACKUP_SRC} ${BACKUP_DST}
