#!/usr/bin/env bash
#
# this configures preview generation to be less system intensive. Speed up
# generation and runtime as the previews are always seen instead in the UI
# instead of the original image.
#
# Refer to: https://ownyourbits.com/2019/06/29/understanding-and-improving-nextcloud-previews/
#
# As Alexey Kachalov says in one comment, only power of 4 makes sense as in
# nextcloud on-the-fly preview generator only power of 4 are generated
#

set -euo pipefail

COMPOSE_FILE="/opt/nextcloud/docker-compose.yml"

docker-compose -f ${COMPOSE_FILE} exec -T -u www-data nextcloud \
    /var/www/html/occ config:app:set previewgenerator squareSizes --value="64 256 1024"

docker-compose -f ${COMPOSE_FILE} exec -T -u www-data nextcloud \
    /var/www/html/occ config:app:set previewgenerator widthSizes  --value="256 1024"

docker-compose -f ${COMPOSE_FILE} exec -T -u www-data nextcloud \
    /var/www/html/occ config:app:set previewgenerator heightSizes --value="256 1024"

docker-compose -f ${COMPOSE_FILE} exec -T -u www-data nextcloud \
    /var/www/html/occ config:system:set preview_max_x --value 2048

docker-compose -f ${COMPOSE_FILE} exec -T -u www-data nextcloud \
    /var/www/html/occ config:system:set preview_max_y --value 2048

docker-compose -f ${COMPOSE_FILE} exec -T -u www-data nextcloud \
    /var/www/html/occ config:system:set jpeg_quality --value 60

docker-compose -f ${COMPOSE_FILE} exec -T -u www-data nextcloud \
    /var/www/html/occ config:app:set preview jpeg_quality --value="60"
