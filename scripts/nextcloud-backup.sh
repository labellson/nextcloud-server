#!/usr/bin/env bash
set -euo pipefail

# backup variables
BACKUP_SRC=""
BACKUP_DST=""
BACKUP_LATEST_LINK="${BACKUP_DST}/latest"

# docker configuration
COMPOSE_FILE=""
NEXT_DB_IMAGE_NAME=""
NEXT_DB_CONTAINER_NAME=""
NEXT_DB_CONTAINER_ENV=""

# 1. set nextcloud in maintenance mode
docker-compose -f ${COMPOSE_FILE} exec -T -u www-data nextcloud /var/www/html/occ maintenance:mode --on

# 2. create destination folder
BACKUP_DST=${BACKUP_DST}/nextcloud-$(date +"%Y-%m-%d")
mkdir -p ${BACKUP_DST}

# 3. incremental backup nextcloud folders
rsync -Aavx ${BACKUP_SRC}/ ${BACKUP_DST}/nextcloud/ \
    --delete-after \
    --link-dest ${BACKUP_LATEST_LINK}/nextcloud

rm ${BACKUP_LATEST_LINK}
ln -s ${BACKUP_DST} ${BACKUP_LATEST_LINK}

# 4. backup postgres cluster globals
docker run --rm \
    --env-file ${NEXT_DB_CONTAINER_ENV} \
    --volume ${BACKUP_DST}:/backup \
    --network container:${NEXT_DB_CONTAINER_NAME} \
    ${NEXT_DB_IMAGE_NAME} \
    sh -c "PGPASSWORD=\${POSTGRES_PASSWORD} pg_dumpall -h ${NEXT_DB_CONTAINER_NAME} -U \${POSTGRES_USER} --globals-only -f /backup/nextcloud-globals.bck"

# 5. backup postgres database
docker run --rm \
    --env-file ${NEXT_DB_CONTAINER_ENV} \
    --volume ${BACKUP_DST}:/backup \
    --network container:${NEXT_DB_CONTAINER_NAME} \
    ${NEXT_DB_IMAGE_NAME} \
    sh -c "PGPASSWORD=\${POSTGRES_PASSWORD} pg_dump \${POSTGRES_DB} -h ${NEXT_DB_CONTAINER_NAME} -U \${POSTGRES_USER} -Fc -f /backup/nextcloud-sqlbck.bck"

# 6. back to normal mode
docker-compose -f ${COMPOSE_FILE} exec -T -u www-data nextcloud /var/www/html/occ maintenance:mode --off
